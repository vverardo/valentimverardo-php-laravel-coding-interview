<?php

namespace Tests\Feature;

use App\Models\Booking;
use App\Models\Client;
use App\Models\Dog;
use Illuminate\Testing\Fluent\AssertableJson;
use PhpParser\Node\Expr\Cast\Bool_;
use Tests\AbstractDatabaseTestCase;

class ClientBookingTest extends AbstractDatabaseTestCase
{
    /**
     * Test client bookings list.
     */
    public function test_returns_a_list_of_client_bookings(): void
    {
        $client = Booking::first()->client;
        $response = $this->getJson("/api/clients/$client->id/bookings");

        $response->assertOk();
        $response->assertJson(fn(AssertableJson $json) => $json->has(Booking::whereBelongsTo($client)->count())
            ->each(fn(AssertableJson $json) => $json->whereAllType([
                'id' => 'integer',
                'price' => ['double', 'integer'],
                'check_in_date' => 'string',
                'check_out_date' => 'string',
                'client_id' => 'integer',
                'created_at' => 'string',
                'updated_at' => 'string',
            ]))
        );
    }

    public function test_can_book_a_new_client(): void
    {
        $client = Client::factory()->create();
        $stubBooking = Booking::factory()->make();

        $response = $this->postJson(
            "/api/clients/{$client->id}/bookings", [
                'price' => $stubBooking->price,
                'check_in_date' => $stubBooking->check_in_date->format('Y-m-d H:i:s'),
                'check_out_date' => $stubBooking->check_out_date->format('Y-m-d H:i:s'),
            ]
        );

        $response->assertCreated();
    }

    public function test_if_the_booking_can_have_discount(): void
    {
        $client = Client::factory()->create();

        Dog::factory()->for($client)->create(['age' => 6]);

        $stubBooking = Booking::factory()->make();

        $priceWithDiscount = $stubBooking->price * Booking::DISCOUNT_TO_APPLY;

        $response = $this->postJson(
            "/api/clients/{$client->id}/bookings", [
                'price' => $stubBooking->price,
                'check_in_date' => $stubBooking->check_in_date->format('Y-m-d H:i:s'),
                'check_out_date' => $stubBooking->check_out_date->format('Y-m-d H:i:s'),
            ]
        );

        $response->assertCreated();

        $response->assertJson( fn(AssertableJson $json) => $json->where('price', $priceWithDiscount)
            ->where('check_in_date',  $stubBooking->check_in_date->format('Y-m-d H:i:s'))
            ->where('check_out_date', $stubBooking->check_out_date->format('Y-m-d H:i:s'))
            ->etc()
        );
    }
}
