<?php

namespace App\Http\Controllers;

use App\Http\Requests\BookingStoreRequest;
use App\Http\Requests\ClientBookingStoreRequest;
use App\Models\Booking;
use App\Models\Client;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\JsonResponse;
use PhpParser\Node\Expr\Cast\Bool_;

class ClientBookingController extends Controller
{
    /**
     * Get client's bookings.
     *
     * @param Client $client
     *
     * @return Collection
     */
    public function index(Client $client): Collection
    {
        return $client->bookings;
    }

    /**
     * Get a booking.
     *
     * @param Booking $booking
     *
     * @return Booking
     */
    public function show(Booking $booking): Booking
    {
        return $booking;
    }

    public function store(ClientBookingStoreRequest $request, Client $client): JsonResponse
    {
        $dogsAvarage = $client->dogs->avg('age');

        $fields = $request->validated();

        if (Booking::DOGS_AGES_TO_DISCOUNT >= $dogsAvarage ) {
            $fields['price'] *= Booking::DISCOUNT_TO_APPLY;
        }

        $booking = $client->bookings()
            ->create($fields);

        return response()->json($booking, 201);
    }
}
