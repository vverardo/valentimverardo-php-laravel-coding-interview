<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Booking extends Model
{
    use HasFactory;

    const DOGS_AGES_TO_DISCOUNT = 10;

    const DISCOUNT_TO_APPLY = 0.9;

    /**
     * @var string[]
     */
    protected $fillable = ['price', 'check_in_date', 'check_out_date'];

    /**
     * The one who booked
     *
     * @return BelongsTo
     */
    public function client(): BelongsTo
    {
        return $this->belongsTo(Client::class);
    }
}
